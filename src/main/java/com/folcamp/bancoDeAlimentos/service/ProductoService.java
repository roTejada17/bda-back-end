package com.folcamp.bancoDeAlimentos.service;

import com.folcamp.bancoDeAlimentos.entity.Producto;
import com.folcamp.bancoDeAlimentos.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    public List<Producto> getAll(){
        List<Producto> list =productoRepository.findAll();
        return list;
    }

    public Optional<Producto> getById(Long id){
        return productoRepository.findById(id);
    }

    public Optional<Producto> getByName(String name){
        return productoRepository.findByName(name);
    }

    public void save(Producto producto){
        productoRepository.save(producto);
    }

    public void delete(Long id){
        productoRepository.deleteById(id);
    }

    public boolean existsById(Long id){
        return productoRepository.existsById(id);
    }

    public boolean existsByName(String name){
        return productoRepository.existsByName(name);
    }
}
