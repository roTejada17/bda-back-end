package com.folcamp.bancoDeAlimentos.controllers;

import com.folcamp.bancoDeAlimentos.dto.Mensaje;
/*import com.folcamp.bancoDeAlimentos.dto.ProductoDto;*/
import com.folcamp.bancoDeAlimentos.entity.Producto;
import com.folcamp.bancoDeAlimentos.service.ProductoService;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
/*import org.springframework.security.access.prepost.PreAuthorize;*/
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("api/producto")
//@CrossOrigin(origins = "http//localhost:4200&quot")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/lista")
    public ResponseEntity<List<Producto>> getList(){
        List<Producto> list = productoService.getAll();
        return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Producto> getById(@PathVariable("id") Long id){
        if(!productoService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        Producto producto = productoService.getById(id).get();
        return new ResponseEntity<Producto>(producto, HttpStatus.OK);
    }

    /*@GetMapping("/detailname/{nombre}")
    public ResponseEntity<Producto> getByName(@PathVariable("nombre") String name){
        if(!productoService.existsByName(name))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        Producto producto = productoService.getByName(name).get();
        return new ResponseEntity(producto, HttpStatus.OK);
    }*/


    @PostMapping("/nuevo")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> create(@RequestBody Producto producto){
        if (StringUtils.isBlank(producto.getName())) {
            return new ResponseEntity(new Mensaje("el nombre es obligatorio"), HttpStatus.BAD_REQUEST);}
            if ((Integer)producto.getPrice() == null || producto.getPrice() <0 )
                return new ResponseEntity(new Mensaje("el precio debe ser mayor que 0"), HttpStatus.BAD_REQUEST);
            if (productoService.existsByName(producto.getName()))
                return new ResponseEntity(new Mensaje("ese nombre ya existe"), HttpStatus.BAD_REQUEST);
            /*Producto producto = new Producto (producto.getName(), producto.getPrice());*/
            productoService.save(producto);
            return new ResponseEntity(new Mensaje("producto creado"), HttpStatus.OK);
        }


    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> update (@RequestBody Producto producto, @PathVariable("id") Long id){
        if(!productoService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        if(productoService.existsByName(producto.getName()) &&
                productoService.getByName(producto.getName())
                .get().getId() != id)
            return new ResponseEntity(new Mensaje("ese nombre ya existe"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(producto.getName()))
            return new ResponseEntity(new Mensaje("el nombre es obligatorio"), HttpStatus.BAD_REQUEST);
        if((Integer)producto.getPrice()==null || producto.getPrice() ==0 )
            return new ResponseEntity(new Mensaje("el precio es obligatorio"), HttpStatus.BAD_REQUEST);

        Producto prodUpdate= productoService.getById(id).get();
        prodUpdate.setName(producto.getName());
        prodUpdate.setPrice(producto.getPrice());
        productoService.save(prodUpdate);
        return new ResponseEntity(new Mensaje("producto actualizado"), HttpStatus.OK);
    }


    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable("id")Long id){
        if(!productoService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        productoService.delete(id);
        return new ResponseEntity(new Mensaje("producto eliminado"), HttpStatus.OK);
    }
}
