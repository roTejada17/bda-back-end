package com.folcamp.bancoDeAlimentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoDeAlimentosApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoDeAlimentosApiApplication.class, args);
	}

}
